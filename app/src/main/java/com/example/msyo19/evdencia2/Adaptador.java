package com.example.msyo19.evdencia2;

import android.annotation.SuppressLint;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

public class Adaptador extends RecyclerView.Adapter<Adaptador.ViewHolderDatos> {
    @NonNull

    ArrayList<Persona> items = new ArrayList<>();

    public Adaptador(ArrayList<Persona> items) {
        this.items = items;
    }

    @Override
    public ViewHolderDatos onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.item,null,false);
        return new ViewHolderDatos(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolderDatos viewHolderDatos, int i) {
        viewHolderDatos.asignar(items.get(i));
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    class ViewHolderDatos extends RecyclerView.ViewHolder {
        TextView nombre,ap,am,s,f,e;

         ViewHolderDatos(@NonNull View itemView) {
            super(itemView);
            nombre= itemView.findViewById(R.id.nombre);
            ap= itemView.findViewById(R.id.ap);
            am= itemView.findViewById(R.id.am);
            s= itemView.findViewById(R.id.s);
            f= itemView.findViewById(R.id.f);
            e= itemView.findViewById(R.id.e);
        }

         @SuppressLint("SetTextI18n")
         void asignar(Persona itemc) {
            nombre.setText("Nombre: "+itemc.getNombre());
            ap.setText("Apellido paterno: "+itemc.getApp());
            am.setText("Apellido materno: "+itemc.getApm());
            s.setText("Sexo: "+itemc.getSexo());
            f.setText("Fecha de nacimiento: "+itemc.getFecha());
            e.setText("Estado de nacimiento: "+itemc.getEstado());
        }
    }
}