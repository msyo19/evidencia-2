package com.example.msyo19.evdencia2;

import android.os.Parcel;
import android.os.Parcelable;

public class Persona implements Parcelable {
    private String nombre,app,apm,sexo,fecha,estado;

    public Persona(String n, String ap, String am, String s, String f, String e){
        this.nombre= n;
        this.app= ap;
        this.apm=am;
        this.sexo=s;
        this.fecha=f;
        this.estado=e;
    }

    public Persona(){

    }

    protected Persona(Parcel in) {
        nombre = in.readString();
        app = in.readString();
        apm = in.readString();
        sexo = in.readString();
        fecha = in.readString();
        estado = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(nombre);
        dest.writeString(app);
        dest.writeString(apm);
        dest.writeString(sexo);
        dest.writeString(fecha);
        dest.writeString(estado);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<Persona> CREATOR = new Creator<Persona>() {
        @Override
        public Persona createFromParcel(Parcel in) {
            return new Persona(in);
        }

        @Override
        public Persona[] newArray(int size) {
            return new Persona[size];
        }
    };

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public String getSexo() {
        return sexo;
    }

    public void setSexo(String sexo) {
        this.sexo = sexo;
    }

    public String getApm() {
        return apm;
    }

    public void setApm(String apm) {
        this.apm = apm;
    }

    public String getApp() {
        return app;
    }

    public void setApp(String app) {
        this.app = app;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String curp(){
        String curp= "";
        curp += (this.app.toUpperCase().toCharArray())[0];
        curp += (this.app.toUpperCase().toCharArray())[1];
        curp += (this.nombre.toUpperCase().toCharArray())[0];
        curp += (this.apm.toUpperCase().toCharArray())[0];
        return curp;
    }
}
