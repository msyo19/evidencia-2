package com.example.msyo19.evdencia2;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class DbH extends SQLiteOpenHelper {

        private static final String DB_NOMBRE="Curps.sqlite";
        private static  int DB_SCHEME_VERSION=1;

        public DbH(Context context) {
            super(context, DB_NOMBRE, null, DB_SCHEME_VERSION);
        }

        @Override
        public void onCreate(SQLiteDatabase db) {
            db.execSQL(DbPersona.CREATE_TABLE);
        }

        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

            db.execSQL("DROP TABLE IF EXISTS "+DB_NOMBRE);

            onCreate(db);

        }

}
