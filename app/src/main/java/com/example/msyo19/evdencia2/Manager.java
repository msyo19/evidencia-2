package com.example.msyo19.evdencia2;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

public abstract class Manager {


        private DbH helper;
        private SQLiteDatabase db;


        Manager(Context ctx) {
            helper= new DbH(ctx);
            db=helper.getWritableDatabase();

        }


        public void cerrar(){
            db.close();
        }


        abstract  void insertarParametros(String n, String ap, String am, String s, String f, String e);

        abstract public void eliminarTodo();
        abstract public Cursor cargarCursor();



        public DbH getHelper() {
            return helper;
        }

        public void setHelper(DbH helper) {
            this.helper = helper;
        }

        public SQLiteDatabase getDb() {
            return db;
        }

        public void setDb(SQLiteDatabase db) {
            this.db = db;
        }

}
